#!/bin/bash

set -e

config="https://kvv.s3.eu-north-1.amazonaws.com/config.txt"

REPO=$(pwd)
clear
printf "\n   repo dir: $REPO \n"

if [[ ! -d "/dev/kvm" ]]
then
  supported=true # KVM supported
  printf "\nKVM seems to be supported\n"
else
  printf "\nKVM is not detected.\nplease make sure KVM is supported and enabled.\n"
  # printf "hello\nworld\n"
fi

if [ "$supported" = true ]
then
  printf "\navailable options:\n\n1) [REQUIRED] install updates and packages\n\n2) install MacOS Catalina [RECOMMENDED]\n\n3) make / fix vnc.sh\n\n"
  read -p "Enter option number: " option
  
  if [ "$option" = "1" ]
  then
    printf "\nupdating repo and upgrading distro\n" # repo update dist upgrade
    apt update && sudo apt upgrade -y
    apt install qemu uml-utilities virt-manager git wget libguestfs-tools p7zip-full -y && printf "\ninstalling required packages\n" # installing virtualization stuff
    printf "\ndone! rebooting the host...\n" && sudo reboot
  fi
  if [ "$option" = "2" ]
  then
    printf "\nchecking config folder\n"
    if [[ ! -d "$REPO/config" ]]
    then
      printf "\nmaking config folder:\n"
      mkdir config && cd config && pwd
      printf "\nfetching config from S3 to $REPO/config:\n"
      curl $config --output config # downloaing image link
      printf "\nresolving image link:\n"
      url=`cat ../config/config` && echo $url
      printf "\ngoing back to $REPO\n" && cd $REPO  # going back to 1clickmac
    else
      read -p "config folder detected. delete? [ENTER]" delete
      if [ "$delete" = "" ]
      then
        rm -r $REPO/config && printf "\ndone deleting $REPO/config\n"
        exit
      fi
    fi
    printf "\nchecking image folder\n"
    if [[ ! -d "$REPO/image" ]]
    then
      printf "\nmaking image folder:\n"
      mkdir image && cd image && pwd
      printf "\ndownloaing macOS image to $REPO/image:\n"
      curl $url --output catalina.qcow2
      printf "\ndone!\n"
    else
      read -p "image folder detected. delete? [ENTER]" delete
      if [ "$delete" = "" ]
      then
        rm -r $REPO/image && printf "\ndone deleting $REPO/config\n"
        exit
      fi
    fi
  fi
  if [ "$option" = "3" ]
  then
    printf "\nchecking vnc.sh\n" # repo update dist upgrade
    if [[ ! -f "$REPO/vnc.sh" ]]
    then
      printf "\nmaking vnc.sh startup script:\n"
      touch vnc.sh
      
    else
      read -p "image folder detected. delete? [ENTER]" delete
      if [ "$delete" = "" ]
      then
        rm -r $REPO/image && printf "\ndone deleting $REPO/config\n"
        exit
      fi
    fi
  fi
fi   
      
      
:'     

if [ "$supported" = true ]
then
  # set link to config
 
  printf "\nMaking config folder...\n" && mkdir config && cd config && pwd # making configurations folder
  # mkdir config && cd config
  # pwd
  
  printf "\ndownloaing config file\n" && curl $config --output config # downloaing image link
  # curl $config --output config

  printf "\nresolving image link\n" # resolving image link
  url=`cat ../config/config` && echo $url
  # echo $url
  
  printf "\ngoing back to 1clickmac\n" && cd ..  # going back to 1clickmac
  # cd .. 
  
  printf "\nmaking folder for image\n" && mkdir image && cd image && pwd # making folder for image
  # mkdir image && cd image 
  # pwd
  
  printf "\ndownloading macOS Catalina (pre-made)\n" && curl $url --output catalina.qcow2  # downloading macOS Catalina (pre-made)
  ## curl $url --output catalina.qcow2 
fi

printf "\ndone\n"
'
